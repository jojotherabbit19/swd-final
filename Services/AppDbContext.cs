﻿using Microsoft.EntityFrameworkCore;
using Services.Entities;
using System.Reflection;

namespace Services
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> op) : base(op) { }
        public DbSet<AbsentRequest> AbsentRequests { get; set; }
        public DbSet<Court> Courts { get; set; }
        public DbSet<CourtSlot> CourtsSlots { get; set; }
        public DbSet<Resevation> Resenesses { get; set; }
        public DbSet<ResevationCourtSlot> ResevationCourtSlots { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<ScheduleDetail> ScheduleDetails { get; set; }
        public DbSet<ScheduleWorkingSlot> ScheduleWorkingSlots { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<WorkingSlot> WorkingSlots { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
