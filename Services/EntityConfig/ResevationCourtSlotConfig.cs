﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.Entities;

namespace Services.EntityConfig
{
    public class ResevationCourtSlotConfig : IEntityTypeConfiguration<ResevationCourtSlot>
    {
        public void Configure(EntityTypeBuilder<ResevationCourtSlot> builder)
        {
            builder.Ignore(x => x.Id);
            builder.HasKey(x => new { x.ResevationId, x.CourtSlotId });
            builder.HasOne(x => x.Resevations).WithMany(x => x.ResevationCourtSlot).HasForeignKey(x => x.ResevationId);
            builder.HasOne(x => x.CourtSlot).WithMany(x => x.ResevationCourtSlot).HasForeignKey(x => x.CourtSlotId);
        }
    }
}
