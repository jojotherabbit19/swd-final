﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Services.Entities;

namespace Services.EntityConfig
{
    public class WorkingSlotScheduleConfig : IEntityTypeConfiguration<ScheduleWorkingSlot>
    {
        public void Configure(EntityTypeBuilder<ScheduleWorkingSlot> builder)
        {
            builder.Ignore(x => x.Id);
            builder.HasKey(x => new { x.ScheduleId, x.WorkingSlotId });
            builder.HasOne(x => x.Schedule).WithMany(x => x.ScheduleWorkingSlots).HasForeignKey(x => x.ScheduleId);
            builder.HasOne(x => x.WorkingSlot).WithMany(x => x.ScheduleWorkingSlots).HasForeignKey(x => x.WorkingSlotId);
        }
    }
}
