﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Services
{
    public static class DependencyInjections
    {
        public static IServiceCollection CoreService(this IServiceCollection services, IConfiguration config)
        {
            //services.AddDbContext<AppDbContext>(op => op.UseSqlServer(config.GetConnectionString("SqlServerDb")));
            services.AddDbContext<AppDbContext>(op => op.UseNpgsql(config.GetConnectionString("PostgresDb")));

            return services;
        }
    }
}
