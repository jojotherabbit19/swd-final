﻿using Services.Entities.Enums;

namespace Services.Entities
{
    public class CourtSlot : BaseEntity
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Status Status { get; set; }
        public decimal BasicPrice { get; set; }
        public decimal TotalPrice
        {
            get
            {
                return decimal.Parse((EndTime - StartTime).ToString()) * BasicPrice;
            }
        }
        public int CourtId { get; set; }
        public Court Court { get; set; }
        public ICollection<ResevationCourtSlot>? ResevationCourtSlot { get; set; }
    }
}
