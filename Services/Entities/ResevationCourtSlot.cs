﻿namespace Services.Entities
{
    public class ResevationCourtSlot : BaseEntity
    {
        public int ResevationId { get; set; }
        public Resevation Resevations { get; set; }
        public int CourtSlotId { get; set; }
        public CourtSlot CourtSlot { get; set; }
    }
}
