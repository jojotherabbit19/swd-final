﻿using Services.Entities.Enums;

namespace Services.Entities
{
    public class ScheduleDetail : BaseEntity
    {
        public int ScheduleId { get; set; }
        public Schedule Schedule { get; set; }
        public Status AttendanceStatus { get; set; }
        public DateTime Date { get; set; }
    }
}
