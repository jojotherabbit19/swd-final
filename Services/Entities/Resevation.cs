﻿using Services.Entities.Enums;

namespace Services.Entities
{
    public class Resevation : BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public Status Status { get; set; }
        public decimal Price { get; set; }
        public BookingStatus BookingStatus { get; set; }
        public int VoucherId { get; set; }
        public Voucher Voucher { get; set; }
        public PaymentType PaymentType { get; set; }
        public ICollection<ResevationCourtSlot>? ResevationCourtSlot { get; set; }
    }
}
