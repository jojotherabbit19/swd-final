﻿using Services.Entities.Enums;

namespace Services.Entities
{
    public class AbsentRequest : BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public string Reason { get; set; }
        public Status Status { get; set; }
        public DateTime Date { get; set; }
    }
}
