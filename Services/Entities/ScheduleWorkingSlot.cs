﻿namespace Services.Entities
{
    public class ScheduleWorkingSlot : BaseEntity
    {
        public int ScheduleId { get; set; }
        public Schedule Schedule { get; set; }
        public int WorkingSlotId { get; set; }
        public WorkingSlot WorkingSlot { get; set; }
    }
}
