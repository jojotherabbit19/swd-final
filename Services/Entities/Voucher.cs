﻿using Services.Entities.Enums;

namespace Services.Entities
{
    public class Voucher : BaseEntity
    {
        public VoucherType VoucherType { get; set; }
        public string? Description { get; set; }
    }
}
