﻿using Services.Entities.Enums;

namespace Services.Entities
{
    public class Court : BaseEntity
    {
        public string Code { get; set; }
        public CourtType CourtType { get; set; }
        public Status Status { get; set; }
        public ICollection<CourtSlot>? CourtSlots { get; set; }
    }
}
