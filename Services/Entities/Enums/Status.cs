﻿namespace Services.Entities.Enums
{
    public enum Status
    {
        Active,
        Inactive,
        Approved,
        Denied,
        Present,
        Absent
    }
}
