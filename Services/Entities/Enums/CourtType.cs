﻿namespace Services.Entities.Enums
{
    public enum CourtType
    {
        Small,
        Medium,
        Large
    }
}
