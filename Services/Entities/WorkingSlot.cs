﻿namespace Services.Entities
{
    public class WorkingSlot : BaseEntity
    {
        public decimal StartTime { get; set; }
        public decimal EndTime { get; set; }
        public decimal Duration
        {
            get
            {
                return EndTime - StartTime;
            }
        }
        public decimal BasicSalary { get; set; }
        public decimal TotalSalary
        {
            get
            {
                return Duration * BasicSalary * Coefficient;
            }
        }
        public decimal Coefficient { get; set; }
        public ICollection<ScheduleWorkingSlot>? ScheduleWorkingSlots { get; set; }
    }
}
