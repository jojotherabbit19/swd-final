﻿using Services.Entities.Enums;

namespace Services.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public DateTime DOB { get; set; }
        public Gender Gender { get; set; }
        public string ImageUrl { get; set; }
        public Status Status { get; set; }
        public Role Role { get; set; }
        public ICollection<Resevation>? Resevations { get; set; }
        public ICollection<Schedule>? Schedules { get; set; }
    }
}
