﻿namespace Services.Entities
{
    public class Schedule : BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int TotalAbsent { get; set; }
        public int TotalPresent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal TotalSalary { get; set; }
        public ICollection<ScheduleWorkingSlot>? ScheduleWorkingSlots { get; set; }
    }
}
